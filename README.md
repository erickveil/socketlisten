constlistener

Project for testing the constant connection feature of the universal listener.

Prompted by a report that data will not transfer continuously on the universal
listener.

This project contains my universal listener class (see the socketclient project).
This is a C++/Qt class that makes setting up consistent network socket
listeners easier for me by merging the 3-5 steps required to start a listener
into one single method call. The only complexity is in how you set up the
class, but even that can be as simple as a constructor that provides the listen
port.

Some flexibility is sacrificed on purpose, as I tend to use socket listeners in
limited ways. The trade off is worthi it to get a simple and consistent
listener setup and lets me keep more of my hair.

