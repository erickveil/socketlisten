#include <QCoreApplication>
#include "textserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    TextServer server;
    server.init();


    return a.exec();
}
