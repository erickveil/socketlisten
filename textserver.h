/**
 * hubstatlink.h
 * Erick Veil
 * 2021-08-23
 * Copyright 2021 Erick Veil
 */
#ifndef TEXTSERVER_H
#define TEXTSERVER_H

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QList>
#include <QObject>
#include <QFile>
#include <QByteArray>

#include "staticlogger.h"
#include "universallistener.h"

/**
 * @brief The TextServer class
 *
 * Adapted from original TextServer class just to make writing the program
 * easier.
 *
 * Changed to only listen on the one port, and to try to maintain a
 * constant connection.
 */
class TextServer : public QObject
{
    Q_OBJECT

    int _port = 50800;

    QList<evtools::UniversalListener*> _listenerPool;

    bool _isInit = false;

public:
    explicit TextServer(QObject *parent = nullptr);
    ~TextServer();

    void init();

private:
    QByteArray _createLocalGetMsg();
    void _initListener(int port);
    QByteArray _listenerAckCb(QByteArray msg);
    QByteArray _createFacDownMsg();

};

#endif // TEXTSERVER_H
