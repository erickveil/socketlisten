/** Copyright 2021 Erick Veil */

#include "textserver.h"

TextServer::TextServer(QObject *parent) : QObject(parent)
{

}

TextServer::~TextServer()
{
    for (int i = 0; i < _listenerPool.size(); ++i) {
        delete _listenerPool.at(i);
    }
}

void TextServer::init()
{
    if (_isInit) { return; }
    _initListener(_port);
    _isInit = true;
}

QByteArray TextServer::_createLocalGetMsg()
{
    QString key = "hubStatusList";
    return key.toLocal8Bit();
}

void TextServer::_initListener(int port)
{
    auto listener = new evtools::UniversalListener();
    listener->IsConstConnection = true;
    _listenerPool.append(listener);
    _listenerPool.last()->setPort(port);
    auto ackCb = [&] (QByteArray msg) {
        return _listenerAckCb(msg);
    };
    _listenerPool.last()->setAckCallback(ackCb);
    _listenerPool.last()->Name = "Const Connect Listener - " +
            QString::number(port);
    _listenerPool.last()->initConnections();
    _listenerPool.last()->startListener();
}

QByteArray TextServer::_listenerAckCb(QByteArray msg)
{
    LOG_INFO("Ack message: " + msg);
    return msg;
}

QByteArray TextServer::_createFacDownMsg()
{
    QJsonObject root;
    QJsonArray statList;
    root["isFacUp"] = false;
    root["statusList"] = statList;
    QJsonDocument doc(root);

    return doc.toJson();
}



